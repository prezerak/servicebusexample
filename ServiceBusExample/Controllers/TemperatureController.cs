﻿using Azure.Messaging.ServiceBus;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ServiceBusExample.Controllers
{
    [Route("api/1.0/[controller]")]
    [ApiController]
    public class TemperatureController : ControllerBase
    {
        private string connectionString = "Endpoint=sb://icepada.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=fQHPm8uJeBGRYmgujhRunP7k2iKYRk8xTheGhbpPehQ=";
        private string queueName = "tempqueue";

        // GET: api/<TemperatureController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Service started !!!");
        }

        

        // POST api/<TemperatureController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MeasurementDTO measurement)
        {
            await using ServiceBusClient client = new ServiceBusClient(connectionString);
            // create a sender for the queue 
            ServiceBusSender sender = client.CreateSender(queueName);

            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(measurement);
            // create a message that we can send
            ServiceBusMessage message = new ServiceBusMessage(jsonString);

            // send the message
            await sender.SendMessageAsync(message);
            Console.WriteLine($"Sent a single message to the queue: {queueName}");

            return Ok();
        }
    }
}
