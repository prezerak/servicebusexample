﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceBusExample
{
    public class MeasurementDTO
    {
        public int Id { get; set; }
        public double Temperature { get; set; }
        public string TimeStamp { get; set;  }
    }
}
