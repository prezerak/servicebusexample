using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace FunctionBusReader
{
    public static class BusReader
    {
        [FunctionName("ReaderFunction")]
        public static void Run([ServiceBusTrigger("tempqueue", Connection = "sbConnection")]string myQueueItem, ILogger log)
        {
            log.LogInformation($"C# ServiceBus queue trigger function processed message: {myQueueItem}");
        }
    }
}
